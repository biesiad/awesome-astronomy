# awesome-astronomy

[![Awesome](https://awesome.re/badge-flat.svg)](https://awesome.re)

😎 Awesome lists about Astronomy stuff

![NASA-Milky-Way](https://github.com/mbiesiad/awesome-astronomy/blob/master/media/GSFC_20171208_Archive_e001738%7Eorig.jpg)

> _Source: NASA - NASA's Hubble Shows Milky Way is Destined for Head-On Collision: https://images.nasa.gov/details-GSFC_20171208_Archive_e001738_



A curated list of assets availible on the Internet related to astronomy.

As you know, that's a large field of science, so any contribution is warmly welcome!

_Inspired by the awesome list thing. You might want to read the complete [awesome list](https://github.com/sindresorhus/awesome)._

# Translations

* [English](https://github.com/mbiesiad/awesome-astronomy)
* [Polish](https://github.com/mbiesiad/awesome-astronomy/tree/pl_PL)

# Content

* [NASA](#nasa)
* [ESA](#esa)
* [Other governments' Agences](#other-governments-agences)
* [Comets](#comets)
* [Asteroids](#asteroids)
* [Sun](#sun)
* [The Moon](#the-moon)
* [Experiments](#experiments)
* [Observatories](#observatories)
* [Exoplanet projects](#exoplanet-projects)
* [Astrophotography](#astrophotography)
* [Society](#society)
* [Citizen science](#citizen-science)
* [Sources for scientific papers](#sources-for-scientific-papers)
* [Code of Conduct](#code-of-conduct)
* [Contributing](#contributing)
* [License](#license)

_____________________________________________________

# NASA

* [NASA](https://www.nasa.gov/) (website) - The National Aeronautics and Space Administration is an independent agency of the United States Federal Government responsible for the civilian space program, as well as aeronautics and aerospace research

* [APOD](https://apod.nasa.gov/apod/astropix.html) (website) - Astronomy Picture of the Day

* [DEVELOP](https://develop.larc.nasa.gov/) - is a NASA Applied Sciences Program that conducts research projects applying NASA Earth observations to environmental policy issues around the globe.

* [NASA Supercomputing](https://www.nas.nasa.gov/SC18/) - NASA supercomputers enable scientific discoveries and engineering triumphs for missions in aeronautics, Earth and space science, & exploration of the universe.

* [NASApeople](https://www.nasa.gov/careers) - Want your career to leave an enduring impact? Join us to explore the extraordinary, every day! 

* [NASA Earth](https://www.nasa.gov/topics/earth/index.html) - NASA uses the vantage point of space to increase our understanding of Earth and improve lives.

* [NASA Climate](https://climate.nasa.gov/) - "Rocket science isn't enough; we're climate scientists, too".

* [HUBBLE SPACE TELESCOPE](https://hubblesite.org/) - is a space telescope that was launched into low Earth orbit in 1990 and remains in operation.

# ESA 

* [ESA](https://www.esa.int/) (website) - The European Space Agency is an intergovernmental organisation of 22 member states dedicated to the exploration of space.

* [Gaia](https://sci.esa.int/web/gaia) - Gaia is an ambitious mission to chart a three-dimensional map of our Galaxy, the Milky Way, in the process revealing the composition, formation and evolution of the Galaxy.

* [ESA Science](http://www.esa.int/Science_Exploration/Space_Science) - Science @ the European Space Agency ESA, keeping you posted on European space science activities.

* [ESA Operations](http://www.esa.int/Enabling_Support/Operations) - From Earth orbit to deep space: sharing the excitement of realtime mission operations at the European Space Agency

# Other Governments' Agences

* [CNSA](http://www.cnsa.gov.cn/) (website) - China National Space Administration

* [POLSA](https://polsa.gov.pl/) (website) - Polish Space Agency

* [CSA](http://www.asc-csa.gc.ca/) (website) - Canadian Space Agency

* [UKSA](https://www.gov.uk/ukspaceagency) - UK Space Agency

* [DLR](http://www.dlr.de/) - The German Aerospace Center

* [ISRO](http://www.isro.gov.in/) - The Indian Space Research Organisation

* [ISA](http://space.gov.il/en) - The Israel Space Agency

* [ASI](http://www.asi.it/) - The Italian Space Agency

* [JAXA](http://www.jaxa.jp/index_e.html) - The Japan Aerospace Exploration Agency

# Comets

![LovejoyComet](https://github.com/mbiesiad/awesome-astronomy/blob/master/media/comet-lovejoy.png)

> Comet Lovejoy2014. Source: [Sky & Telescope](https://skyandtelescope.org/online-gallery/comet-lovejoy-15/)

* [ICQ](http://www.icq.eps.harvard.edu/) - Comet Information and the International Comet Quarterly (ICQ).

* [MPC](https://www.minorplanetcenter.net/iau/mpc.html) - The MPC is responsible for the designation of minor bodies in the solar system: minor planets; comets; and natural satellites

# Asteroids

* [TOTAS](https://totas.cosmos.esa.int/) - Teide Observatory Tenerife Asteroid Survey

* [ATLAS](http://www.fallingstar.com) - Asteroid Terrestrial-impact Last Alert System: A NASA and University of Hawaii project to patrol the sky every night in search of incoming asteroids.

* [Pan-STARRS](http://pswww.ifa.hawaii.edu/pswww) - The Panoramic Survey Telescope and Rapid Response System

# Sun

![NASA-Sun-Corona](https://github.com/mbiesiad/awesome-astronomy/blob/master/media/corona2.en.jpg)

> _Image of corona from NASA's Solar Dynamics Observatory showing features created by magnetic fields. Image credit: NASA https://spaceplace.nasa.gov/sun-corona/en/_

* [SOHO](https://sohowww.nascom.nasa.gov/) - The Solar and Heliospheric Observatory

* [LASCO](https://lasco-www.nrl.navy.mil/index.php?p=content/about_lasco) - The Large Angle and Spectrometric Coronagraph

# The Moon

![Moon-Nasa](https://github.com/mbiesiad/awesome-astronomy/blob/master/media/moon-nasa.png)

> _A full moon captured July 18, 2008. Credit: NASA/Sean Smith https://asd.gsfc.nasa.gov/blueshift/index.php/2015/02/12/no-mardi-gras-under-a-full-moon/_

* [Moon Society](https://www.moonsociety.org/) - space advocacy organization, founded in 2000, and dedicated to promoting large-scale human exploration, research, and settlement of the Moon.

# Experiments

* [LIGO](https://www.ligo.caltech.edu/) - large-scale physics experiment and observatory to detect cosmic gravitational waves and to develop gravitational-wave observations as an astronomical tool.

* [ET](http://www.et-gw.eu/) - Einstein Telescope (ET) or Einstein Observatory, is a proposed third-generation ground-based gravitational wave detector, currently under study by some institutions in the European Union.

# Observatories

* [VO](http://ivoa.net/) - Virtual observatory. Collection of interoperating data archives and software tools which utilize the internet to form a scientific research environment in which astronomical research programs can be conducted.

# Exoplanet projects

* [Exoplanet Exploration Program](https://exoplanets.nasa.gov/) - Exoplanet Exploration Program. NASA's search for habitable planets and life beyond our solar system.
* [SRON](https://www.sron.nl/exoplanets) - SRON's Exoplanets programme.
* [NASAExoplanetArchive](https://exoplanetarchive.ipac.caltech.edu/) - A service of NASA Exoplanet Science Institute
* [MOA](http://www.phys.canterbury.ac.nz/moa/) - Microlensing Observations in Astrophysics
* [List of exoplanet search projects](https://en.wikipedia.org/wiki/List_of_exoplanet_search_projects) - List of exoplanet search projects by Wikipedia.

# Astrophotography

* [Astrophotography/ Amateur Astronomy Enthusiasts](https://www.facebook.com/groups/astrophotographers/) - Facebook' group
* [Astrophotography tips](https://skyandtelescope.org/astronomy-resources/astrophotography-tips/) - Astrophotography: Tips & Techniques (skyandtelescope.org)
* [Astrophotography how-to](http://www.astropix.com/html/i_astrop/toc_ap.html) - Catching the Light. Astrophotography by Jerry Lodriguss

# Society

Astronomy-related societies, organisations etc on the world.

## Europe

### Poland

* [SRC](http://www.cbk.waw.pl/) - The Space Research Centre

* [TOS](https://pl.wikipedia.org/wiki/Towarzystwo_Obserwator%C3%B3w_S%C5%82o%C5%84ca) - Solar Observers Society (pol. Towarzystwo Obserwatorów Słońca)

* [PTA](http://www.pta.edu.pl/) - The Polish Astronomical Society (pol. Polskie Towarzystwo Astronomiczne)

* [PTF](http://www.ptf.net.pl/) - The Polish Physical Society (pol. Polskie Towarzystwo Fizyczne)

* [PTMA](http://ptma.pl/) - Polish Society of Amateur Astronomers (pol. Polskie Towarzystwo Miłośników Astronomii)

* [Urania – Postępy Astronomii](http://www.urania.edu.pl/) - Polish astronomical magazine

* [PKiM](http://www.pkim.org/) - Polish Fireball Network

## International

* [IAU](https://www.iau.org/) - International Astronomical Union

* [MPC](https://www.minorplanetcenter.net/iau/mpc.html) - Minor Planet Center is the official worldwide organization in charge of collecting observational data for minor planets (such as asteroids), calculating their orbits and publishing this information via the Minor Planet Circulars.

* [CBAT](http://www.cbat.eps.harvard.edu/) - The Central Bureau for Astronomical Telegrams is the official international clearing house for information relating to transient astronomical events. The CBAT collects and distributes information on comets, natural satellites, novae, supernovae and other transient astronomical events.

* [ATel](http://www.astronomerstelegram.org/) - The Astronomer's Telegram is an internet based short notice publication service for quickly disseminating information on new astronomical observations.

* [JPL](https://ssd.jpl.nasa.gov/) - This site provides information related to the orbits, physical charateristics, and discovery circumstances for most known natural bodies in orbit around our sun.

* [ESO](https://www.eso.org/public/) - The European Organisation for Astronomical Research in the Southern Hemisphere, is a 16-nation intergovernmental research organisation for ground-based astronomy.

* [Mars Society](https://www.marssociety.org/) - is an American worldwide volunteer-driven space-advocacy non-profit organization dedicated to promoting the human exploration and settlement of the planet Mars.

* [Astronomy](https://astronomy.com/) - (ISSN 0091-6358) is a monthly American magazine about astronomy.

* [S&T](https://skyandtelescope.org/) - Sky & Telescope is a monthly American magazine covering all aspects of amateur astronomy.

* [CERN](https://home.cern/) - the European Organization for Nuclear Research, is the world's largest particle physics lab.

# Citizen science

Citizen science (via [Wikipedia](https://en.wikipedia.org/wiki/Citizen_science)) is scientific research conducted, in whole or in part, by amateur (or nonprofessional) scientists.

## Sungrazer

![Sungrazer-Lovejoy](https://github.com/mbiesiad/awesome-astronomy/blob/master/media/sungrazer-lovejoy.png)

> _C/2011 W3 (Lovejoy) - SOHO/LASCO C3 (2011/12/15) - Credits: NASA, ESA (LASCO C3)_

* [Sungrazer](https://sungrazer.nrl.navy.mil/) (website) - A NASA-funded Citizen Science Project that enables anyone in the world to become a comet discoverer!

## Zooniverse

* [Zooniverse](https://www.zooniverse.org/) (website) - is a citizen science web portal owned and operated by the Citizen Science Alliance

* [Zooniverse repo on GitHub](https://github.com/zooniverse) - Zooniverse' repositories on the GitHub

(selected Space & Physics projects):

* Galaxy Zoo [(website)](https://www.zooniverse.org/projects/zookeeper/galaxy-zoo)
* Variable Star Zoo [(website)](https://www.zooniverse.org/projects/ilacerna/variable-star-zoo)
* SuperWASP Variable Stars [(website)](https://www.zooniverse.org/projects/ajnorton/superwasp-variable-stars)
* Planet Hunters TESS [(website)](https://www.zooniverse.org/projects/nora-dot-eisner/planet-hunters-tess)
* Muon Hunters 2.0 [(website)](https://www.zooniverse.org/projects/dwright04/muon-hunters-2-dot-0)
* Hubble Asteroid Hunter [(website)](https://www.zooniverse.org/projects/sandorkruk/hubble-asteroid-hunter)
* Galaxy Zoo Mobile [(website)](https://www.zooniverse.org/projects/mikewalmsley/galaxy-zoo-mobile)
* Solar Stormwatch II [(website)](https://www.zooniverse.org/projects/shannon-/solar-stormwatch-ii)
* Backyard Worlds: Planet 9 [(website)](https://www.zooniverse.org/projects/marckuchner/backyard-worlds-planet-9)
* Gravity Spy [(website)](https://www.zooniverse.org/projects/zooniverse/gravity-spy)
* Radio Meteor Zoo [(website)](https://www.zooniverse.org/projects/zooniverse/radio-meteor-zoo)
* Comet Hunters [(website)](https://www.zooniverse.org/projects/mschwamb/comet-hunters)
* Pulsar Hunters [(website)](https://www.zooniverse.org/projects/zooniverse/pulsar-hunters)
* Supernova Hunters [(website)](https://www.zooniverse.org/projects/dwright04/supernova-hunters)
* Disk Detective [(website)](https://www.diskdetective.org/#/)
* Planet Hunters TESS [(website)](https://www.zooniverse.org/projects/nora-dot-eisner/planet-hunters-tess#/)
* Higgs Hunters [(website)](https://www.higgshunters.org/#/)
* Galaxy Zoo: Bar Lengths [(website)](https://www.zooniverse.org/projects/vrooje/galaxy-zoo-bar-lengths)
* Milky Way Project [(website)](https://www.zooniverse.org/projects/povich/milky-way-project)
* Radio Galaxy Zoo [(website)](https://radio.galaxyzoo.org/#/)
* Astronomy Rewind [(website)](https://www.zooniverse.org/projects/zooniverse/astronomy-rewind)
* Radio Meteor Zoo [(website)](https://www.zooniverse.org/projects/zooniverse/radio-meteor-zoo)

# Sources for scientific papers

* [arXiv](https://arxiv.org/) - is a free distribution service and an open-access archive for scholarly articles in the fields of physics, mathematics, computer science, quantitative biology, quantitative finance, statistics, electrical engineering and systems science, and economics. 

* [ADS](https://ui.adsabs.harvard.edu/) - Astrophysics Data System is an online database of over eight million astronomy and physics papers from both peer reviewed and non-peer reviewed sources.

* [ResearchGate](https://www.researchgate.net/) - is a European commercial social networking site for scientists and researchers to share papers, ask and answer questions, and find collaborators.

# Tools

* [FITS](https://en.wikipedia.org/wiki/FITS) - Flexible Image Transport System (FITS) is an open standard defining a digital file format useful for storage, transmission and processing of data: formatted as multi-dimensional arrays (for example a 2D image), or tables (Filename extensions: .fits, .fit, .fts).

* [LaTeX](https://en.wikipedia.org/wiki/LaTeX) - is a document preparation system.

* [Python](https://www.python.org/) - is an interpreted, high-level, general-purpose programming language.

* [R](https://www.r-project.org/) - is a programming language and free software environment for statistical computing and graphics supported by the R Foundation for Statistical Computing.

# Social media & groups

Astro groups on Facebook, Yahoo or groups.io etc

## Twitter

* [TheMarsSociety](https://twitter.com/TheMarsSociety)
* [ATLAS Project](https://twitter.com/fallingstarIfA)
* [NASA_DEVELOP](https://twitter.com/NASA_DEVELOP)
* [ASAS-SN](https://twitter.com/SuperASASSN)
* [NASA Supercomputing](https://twitter.com/NASA_Supercomp)
* [NASA People](https://twitter.com/NASApeople)
* [NASA Goddard Images](https://twitter.com/NASAGoddardPix)
* [NASA HQ PHOTO](https://twitter.com/nasahqphoto)
* [NASA Marshall](https://twitter.com/NASA_Marshall)
* [ATel](https://twitter.com/astronomerstel)
* [NASA Tournament Lab](https://twitter.com/NASA_NTL)
* [ESA EarthObservation](https://twitter.com/ESA_EO)
* [ESA Exhibitions](https://twitter.com/ESAExhibitions)
* [Astronomy Magazine](https://twitter.com/AstronomyMag)
* [Sky & Telescope](https://twitter.com/SkyandTelescope)
* [NASA Earth](https://twitter.com/NASAEarth)
* [NASA Ice](https://twitter.com/NASA_ICE)
* [NASA Climate](https://twitter.com/NASAClimate)
* [CERN](https://twitter.com/CERN)
* [Scientific American](https://twitter.com/sciam)
* [NASA JPL](https://twitter.com/NASAJPL)
* [NASA's Kennedy Space Center](https://twitter.com/NASAKennedy)
* [LIGO](https://twitter.com/LIGO)
* [NASA Glenn Research](https://twitter.com/NASAglenn)
* [ESA Science](https://twitter.com/esascience)
* [ESA Operations](https://twitter.com/esaoperations)
* [NASA Solar System](https://twitter.com/NASASolarSystem)
* [NASA Astronauts](https://twitter.com/NASA_Astronauts)
* [Pan-STARRS1 NEOwatch](https://twitter.com/PS1NEOwatch)

## Facebook

* Comets and Asteroids (page) - [link](https://www.facebook.com/Comets-and-Asteroids-140234731687/)
* The Moon Appreciation Society (page) - [link](https://www.facebook.com/MoonAppSoc/)

## Groups

* Comets and Asteroids Group (Facebook) - [link](https://www.facebook.com/groups/1454757177944714/)

# Contributing

Warmly welcome! Kindly go through [Contribution Guidelines](CONTRIBUTING.md) first.

# Code of Conduct

Examples of behavior that contributes to creating a positive environment include:

    Using welcoming and inclusive language
    Being respectful of differing viewpoints and experiences
    Gracefully accepting constructive criticism
    Focusing on what is best for the community
    Showing empathy towards other community members

# License
Free [MIT](LICENSE) license.

__________________________________________________

Created by @[mbiesiad](https://github.com/mbiesiad)
